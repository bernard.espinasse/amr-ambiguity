
********************************************************************************
*   CORPUS OF AMBIGUOUS SENTENCES FOR AMR EVALUATION
*  
* For more information, contact:
*    - Bernard ESPINASSE (bernard.espinasse@lis-lab.fr)
*    - Rita HIJAZI (rita.hijazi@etu.univ-amu.fr )
********************************************************************************
The file AmbiguousCorpusAMR.txt propose a corpus of ambiguous sentences in English, each with an AMR representation (in Penman notation) developed and validated by human annotators. 

The corpus consists of fifteen statements that present lexical and syntactic ambiguities collected in several works of linguistics and grammar, with an average of 8 words per sentence. 

The ambiguities of this corpus are of the following types:

- Sentences with lexical categorial ambiguities

- Sentences with lexical non-categorial ambiguities

- Sentences with syntactical (structural) ambiguities

The objective was to see if AMR parsers are able to deal with these kinds of ambiguities. 